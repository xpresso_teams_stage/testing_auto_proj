import os
import sys
import dill
import pickle
import urllib
import sklearn
import tarfile
import warnings
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from sklearn.pipeline import Pipeline
from sklearn.impute import SimpleImputer
from pandas.plotting import scatter_matrix
from sklearn.compose import ColumnTransformer
from sklearn.metrics import mean_squared_error
from sklearn.tree import DecisionTreeRegressor
from sklearn.preprocessing import OneHotEncoder
from sklearn.metrics import mean_absolute_error
from sklearn.preprocessing import OrdinalEncoder
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.model_selection import StratifiedShuffleSplit
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot
from xpresso.ai.core.commons.exceptions.xpr_exceptions import MountPathNoneType


if not os.environ["XPRESSO_MOUNT_PATH"]:
    raise MountPathNoneType
PICKLE_PATH = os.path.join(os.environ["XPRESSO_MOUNT_PATH"], "xjp_store")

if not os.path.exists(PICKLE_PATH):
    os.makedirs(PICKLE_PATH, mode=0o777, exist_ok=True)

try:
    housing = pickle.load(open(f"{PICKLE_PATH}/housing.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    train_set = pickle.load(open(f"{PICKLE_PATH}/train_set.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    test_set = pickle.load(open(f"{PICKLE_PATH}/test_set.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))

## $xpr_param_component_name = data_versioning_random
## $xpr_param_component_type = pipeline_job
## $xpr_param_global_variables = ["housing", "train_set", "test_set"]



train_set, test_set = train_test_split(housing, test_size=0.2, random_state=42)
test_set.head()

try:
    pickle.dump(housing, open(f"{PICKLE_PATH}/housing.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(train_set, open(f"{PICKLE_PATH}/train_set.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(test_set, open(f"{PICKLE_PATH}/test_set.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))

